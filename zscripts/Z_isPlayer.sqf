// Headless client check and playerClient array builder
//Originally created by the MSO team under an Apache licence and released under same license http://dev.withsix.com/projects/mso
//Modified by Zorrobyte to build playerClient array,pVar
isHC = false;

if(isNil "headlessClients" && isServer) then {
        headlessClients = [];
		playerClients = [];
        publicVariable "headlessClients";
		publicVariable "playerClients";
};

waitUntil{!isNil "headlessClients"};
waitUntil{!isNil "playerClients"};

if (!isDedicated) then {
//if player client then (not server)
        private["_hc","_lock"];
        _hc = ppEffectCreate ["filmGrain", 2005];
//Create effect on all clients
        if (_hc == -1) then {
                isHC = true;
//If any client not 3d renderer, then set isHC true
		// Random delay
		 for [{_x=1},{_x<=random 10000},{_x=_x+1}] do {};
                if(!(player in headlessClients)) then {
//if NOT (player in headlessClients) then do 
                        headlessClients set [count headlessClients, player];
//set array headlessClients index headlessClients add player
                        publicVariable "headlessClients";
						diag_log headlessClients;
                };
        } else {
				playerClients set [count playerClients, player];
                ppEffectDestroy _hc;
				publicVariable "playerClients";
				diag_log playerClients;
        };
};
isHC;