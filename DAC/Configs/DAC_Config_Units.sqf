//////////////////////////////
//    Dynamic-AI-Creator    //
//    Version 2.1 - 2009    //
//--------------------------//
//    DAC_Config_Units      //
//--------------------------//
//    Script by Silola      //
//    silola@freenet.de     //
//////////////////////////////

private ["_TypNumber","_TempArray","_Unit_Pool_S","_Unit_Pool_V","_Unit_Pool_T","_Unit_Pool_A"];
_TypNumber = _this select 0;_TempArray = [];

switch (_TypNumber) do
{
//-------------------------------------------------------------------------------------------------
// OPF_F
  case 0:
  {
    _Unit_Pool_S = ["O_helipilot_F","O_crew_F","O_Soldier_TL_F","O_engineer_F","O_helicrew_F","O_Pilot_F","O_medic_F","O_officer_F","O_Soldier_A_F","O_Soldier_AA_F","O_Soldier_AR_F","O_Soldier_AT_F","O_soldier_exp_F","O_Soldier_F","O_Soldier_GL_F","O_Soldier_LAT_F","O_Soldier_lite_F","O_soldier_M_F","O_soldier_PG_F","O_soldier_repair_F","O_Soldier_SL_F","O_soldier_UAV_F"];
    _Unit_Pool_V = ["O_MRAP_02_F","O_MRAP_02_gmg_F","O_MRAP_02_hmg_F","O_Quadbike_01_F","O_Truck_02_covered_F","O_Truck_02_transport_F","O_Truck_02_Ammo_F","O_Truck_02_box_F","O_Truck_02_fuel_F","O_Truck_02_medical_F"];
    _Unit_Pool_T = ["O_APC_Tracked_02_AA_F","O_APC_Tracked_02_cannon_F","O_APC_Wheeled_02_rcws_F","O_MBT_02_arty_F","O_MBT_02_cannon_F"];
    _Unit_Pool_A = ["O_Heli_Attack_02_black_F","O_Heli_Attack_02_F","O_Heli_Light_02_F","O_Heli_Light_02_unarmed_F"];
  };
//-------------------------------------------------------------------------------------------------
// BLU_F
  case 1:
  {
    _Unit_Pool_S = ["B_Helipilot_F","B_crew_F","B_Soldier_TL_F","B_engineer_F","B_helicrew_F","B_Pilot_F","B_medic_F","B_officer_F","B_Soldier_A_F","B_soldier_AA_F","B_soldier_AR_F","B_soldier_AT_F","B_soldier_exp_F","B_Soldier_F","B_Soldier_GL_F","B_soldier_LAT_F","B_Soldier_lite_F","B_soldier_M_F","B_soldier_PG_F","B_soldier_repair_F","B_Soldier_SL_F","B_soldier_UAV_F"];
    _Unit_Pool_V = ["B_MRAP_01_F","B_MRAP_01_gmg_F","B_MRAP_01_hmg_F","B_Quadbike_01_F","B_Truck_01_box_F","B_Truck_01_covered_F","B_Truck_01_mover_F","B_Truck_01_transport_F","B_Truck_01_ammo_F","B_Truck_01_fuel_F","B_Truck_01_medical_F","B_Truck_01_Repair_F"];
    _Unit_Pool_T = ["B_APC_Tracked_01_AA_F","B_APC_Tracked_01_rcws_F","B_APC_Wheeled_01_cannon_F","B_MBT_01_arty_F","B_MBT_01_cannon_F","B_MBT_01_mlrs_F"];
    _Unit_Pool_A = ["B_Heli_Attack_01_F","B_Heli_Light_01_armed_F","B_Heli_Light_01_F","B_Heli_Transport_01_camo_F","B_Heli_Transport_01_F"];
  };
//-------------------------------------------------------------------------------------------------
// IND_F
  case 2:
  {
    _Unit_Pool_S = ["I_helipilot_F","I_crew_F","I_Soldier_TL_F","I_engineer_F","I_helicrew_F","I_Pilot_F","I_medic_F","I_officer_F","I_Soldier_A_F","I_Soldier_AA_F","I_Soldier_AR_F","I_Soldier_AT_F","I_Soldier_exp_F","I_soldier_F","I_Soldier_GL_F","I_Soldier_LAT_F","I_Soldier_lite_F","I_Soldier_M_F","I_Soldier_repair_F","I_Soldier_SL_F","I_soldier_UAV_F"];
    _Unit_Pool_V = ["I_MRAP_03_F","I_MRAP_03_gmg_F","I_MRAP_03_hmg_F","I_Quadbike_01_F","I_Truck_02_covered_F","I_Truck_02_transport_F","I_Truck_02_ammo_F","I_Truck_02_box_F","I_Truck_02_fuel_F","I_Truck_02_medical_F"];
    _Unit_Pool_T = ["I_APC_tracked_03_cannon_F","I_APC_Wheeled_03_cannon_F","I_MBT_03_cannon_F"];
    _Unit_Pool_A = ["I_Heli_light_03_F","I_Heli_light_03_unarmed_F","I_Heli_Transport_02_F"];
  };
//-------------------------------------------------------------------------------------------------
// CIV_F
  case 3:
  {
    _Unit_Pool_S = ["C_man_1","C_man_1_1_F","C_man_1_2_F","C_man_1_3_F","C_man_hunter_1_F","C_man_p_beggar_F","C_man_p_fugitive_F","C_man_p_shorts_1_F","C_man_pilot_F","C_man_polo_1_F","C_man_polo_2_F","C_man_polo_3_F","C_man_polo_4_F","C_man_polo_5_F","C_man_polo_6_F","C_man_shorts_1_F","C_man_shorts_2_F","C_man_shorts_3_F","C_man_shorts_4_F","C_man_w_worker_F"];
    _Unit_Pool_V = ["C_Hatchback_01_F","C_Hatchback_01_sport_F","C_Offroad_01_F","C_Quadbike_01_F","C_SUV_01_F","C_Van_01_box_F","C_Van_01_transport_F"];
    _Unit_Pool_T = ["C_Van_01_fuel_F"];
    _Unit_Pool_A = [];
  };
//-------------------------------------------------------------------------------------------------
  Default
  {
    if(DAC_Basic_Value != 5) then
    {
      DAC_Basic_Value = 5;publicvariable "DAC_Basic_Value";
      hintc "Error: DAC_Config_Units > No valid config number";
    };
    if(true) exitwith {};
  };
};

if(count _this == 2) then
{
  _TempArray = _TempArray + [_Unit_Pool_S,_Unit_Pool_V,_Unit_Pool_T,_Unit_Pool_A];
}
else
{
  _TempArray = _Unit_Pool_V + _Unit_Pool_T + _Unit_Pool_A;
};
_TempArray