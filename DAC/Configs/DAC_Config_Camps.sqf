//////////////////////////////
//    Dynamic-AI-Creator    //
//    Version 3.0 - 2010    //
//--------------------------//
//    DAC_Config_Camps      //
//--------------------------//
//    Script by Silola      //
//    silola@freenet.de     //
//////////////////////////////

private [
			"_CampTyp","_campBasic","_campAmmo","_campStatic","_campWall","_campObjInit",
			"_campUserObj","_campAddUnit","_campRandomObj","_Unit_Pool_C","_array"
		];

			_CampTyp = _this select 0;_array = [];

switch (_CampTyp) do
{
//-------------------------------------------------------------------------------------------------------------------------
	//OPF_F
	case 0:
	{
		_campBasic     = ["Flag_CSAT_F",["Campfire_burning_F",8,5,0],["Land_TentA_F",5,0,0],["Logic",10,15,0],0];
		_campAmmo      = [["Box_East_Wps_F",10,2,0],["Box_East_WpsLaunch_F",10,0,0],["Box_East_WpsSpecial_F",10,-2,0],["Box_East_Ammo_F",10,-4,0]];
		_campStatic    = [["O_static_AA_F",-7,25,0,"O_Soldier_GL_F"],["O_static_AT_F",25,25,0,"O_Soldier_GL_F"],["O_Mortar_01_F",25,-20,180,"O_Soldier_GL_F"],["O_GMG_01_high_F",-7,-20,180,"O_Soldier_GL_F"]];
		_campAddUnit   = [];
		_campUserObj   = [["Land_Loudspeakers_F",5,-5,45]];
		_campRandomObj = [];
		_campWall      = ["Land_BagFence_Long_F",[-10,30],[40,56,0],[5,5,5,5],[1,0.2],[0,0]];
		_campObjInit   = [[],[],[],[],[],[],[]];
	};
//-------------------------------------------------------------------------------------------------------------------------
	//BLU_F
	case 1:
	{
		_campBasic     = ["Flag_NATO_F",["Campfire_burning_F",8,5,0],["Land_TentDome_F",5,0,0],["Logic",10,15,0],0];
		_campAmmo      = [["Box_NATO_WpsLaunch_F",10,2,0],["Box_NATO_WpsSpecial_F",10,0,0],["Box_NATO_Ammo_F",10,-2,0],["Box_NATO_Wps_F",10,-4,0]];
		_campStatic    = [["B_static_AA_F",-7,25,0,"B_Soldier_GL_F"],["B_static_AT_F",25,25,0,"B_Soldier_GL_F"],["B_Mortar_01_F",25,-20,180,"B_Soldier_GL_F"],["B_GMG_01_high_F",-7,-20,180,"B_Soldier_GL_F"]];
		_campAddUnit   = [];
		_campUserObj   = [["Land_Loudspeakers_F",5,-5,45]];
		_campRandomObj = [];
		_campWall      = ["Land_BagFence_Long_F",[-10,30],[40,56,0],[5,5,5,5],[1,0.2],[0,0]];
		_campObjInit   = [[],[],[],[],[],[],[]];
	};
//-------------------------------------------------------------------------------------------------------------------------
	//IND_F
	case 2:
	{
		_campBasic     = ["Flag_FIA_F",["Campfire_burning_F",8,5,0],["Land_TentA_F",5,0,0],["Logic",10,15,0],0];
		_campAmmo      = [["Box_IND_WpsLaunch_F",10,2,0],["Box_IND_Ammo_F",10,0,0],["Box_IND_Wps_F",10,-2,0],["Box_IND_WpsSpecial_F",10,-4,0]];
		_campStatic    = [["I_static_AA_F",-7,25,0,"I_Soldier_GL_F"],["I_static_AT_F",25,25,0,"I_Soldier_GL_F"],["I_Mortar_01_F",25,-20,180,"I_Soldier_GL_F"],["I_GMG_01_high_F",-7,-20,180,"I_Soldier_GL_F"]];
		_campAddUnit   = [];
		_campUserObj   = [["Land_Loudspeakers_F",5,-5,45]];
		_campRandomObj = [];
		_campWall      = ["Land_BagFence_Long_F",[-10,30],[40,56,0],[5,5,5,5],[1,0.2],[0,0]];
		_campObjInit   = [[],[],[],[],[],[],[]];
	};
//-------------------------------------------------------------------------------------------------------------------------
	//Add other factions below
	case 3:
	{
		_campBasic     = ["NULL"];
		_campAmmo      = [];
		_campStatic    = [];
		_campAddUnit   = [];
		_campUserObj   = [];
		_campRandomObj = [];
		_campWall      = [];
		_campObjInit   = [[],[],[],[],[],[],[]];
	};
//-------------------------------------------------------------------------------------------------------------------------
	case 4:
	{
		_campBasic     = ["NULL"];
		_campAmmo      = [];
		_campStatic    = [];
		_campAddUnit   = [];
		_campUserObj   = [];
		_campRandomObj = [];
		_campWall      = [];
		_campObjInit   = [[],[],[],[],[],[],[]];
	};
//-------------------------------------------------------------------------------------------------------------------------
	case 5:
	{
		_campBasic     = ["NULL"];
		_campAmmo      = [];
		_campStatic    = [];
		_campAddUnit   = [];
		_campUserObj   = [];
		_campRandomObj = [];
		_campWall      = [];
		_campObjInit   = [[],[],[],[],[],[],[]];
	};
//-------------------------------------------------------------------------------------------------------------------------
	case 11:
	{
		_campBasic     = ["NULL"];
		_campAmmo      = [];
		_campStatic    = [];
		_campAddUnit   = [];
		_campUserObj   = [];
		_campRandomObj = [];
		_campWall      = [];
		_campObjInit   = [[],[],[],[],[],[],[]];
	};
//-------------------------------------------------------------------------------------------------------------------------
	Default {
				if(DAC_Basic_Value != 5) then
				{
					DAC_Basic_Value = 5;publicvariable "DAC_Basic_Value";
					hintc "Error: DAC_Config_Camps > No valid config number";
				};
				if(true) exitwith {};
			};
};

_array = [_campBasic,_campAmmo,_campStatic,_campAddUnit,_campUserObj,_campRandomObj,_campWall,_campObjInit];
_array